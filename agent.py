from random import uniform
import numpy as np

class Agent:

    def __init__(
            self, 
            id: int, 
            n_agents: int, 
            agents: list,
            a: float = 7,
            b: float = 3
        ) -> None:

        self.id = id
        self.n_agents = n_agents
        self.agents = agents
        self.liar = False
        self.recieved_messages = [Agent]
        self.answers = [Agent]
        self.opinions = {}
        self.alpha = 0.25
        self.a = a
        self.b = b

    def init_opinion(self) -> None:
        for agent in self.agents:
            # initialize the opinions for every other agent
            # [0] = belief
            # [1] = disbelief
            # [2] = uncertanty
            # [3] = base rate (greter it is, faster the agent change its opinion)
            self.opinions[agent.id] = (0, 0, 1, self.alpha)

    def send_message(self) -> int:
        if uniform(0, 1) < 0.5:
            for agent in self.agents:
                if agent.id != self.id:
                        agent.recieved_messages.append(self.id)
            return 1
        return 0

    def answer(self) -> None:
        for message in self.recieved_messages:
            if uniform(0, 1) > 0.8:
                # there is a chance of the agent doesn't answer the question
                # that is the same as givin a bad answer
                self.answers.append((message, 0.3))
            else:
                self.answers.append((message, self.generate_answer())) 
        

    def generate_answer(self) -> float:
        return np.random.beta(self.a, self.b)
    
    def vote(self) -> int:
        return self.__choose_liar()

    def get_trustworthiness(self, agent_id):
        b, _, u, a = self.opinions[agent_id]
        # trustworthiness = belifes + uncertanty * base_rate
        # trustworthiness is between 0 and 1
        return np.clip(0, 1, b + u*a)
    

    def __choose_liar(self) -> int:
        sus = None
        for agent in self.agents:
            
            if agent.id == self.id:
                continue

            self.update_opinion(agent)

            if sus is None or \
               self.get_trustworthiness(agent.id) < self.get_trustworthiness(sus.id):
                sus = agent

        return sus.id

    def update_opinion(self, agent):

        # get new opinion based on current answers
        opinion = self.opinions[agent.id]
        
        evidences = (0, 0, 1, self.alpha)
        for asker_id, answer in agent.answers:
            if asker_id == self.id:
                # update opnions that occur at the same time
                evidences = self.avg_fusion(evidences, self.evidence(answer))
            else:
                # apply the discount factor
                discounted_opinion = self.discount_factor(asker_id, self.evidence(answer))
                evidences = self.avg_fusion(evidences, discounted_opinion)

        # update opinion based on new evidences
        self.opinions[agent.id] = self.cumulative_fusion(opinion, evidences)

    def cumulative_fusion(self, prev_opinion, new_opinion):
        belif_a, disbel_a, uncer_a, alpha_a = prev_opinion
        belif_b, disbel_b, uncer_b, alpha_b = new_opinion


        if uncer_a and uncer_b == 0:
            belif = (belif_a + belif_b) / 2.
            disbel = (disbel_a + disbel_b) / 2.
            uncer = 0
            alpha = (alpha_a + alpha_b) / 2.
        else:
            belif = (belif_a * uncer_b + belif_b * uncer_a) \
                  / (uncer_a + uncer_b - uncer_a*uncer_b)
            
            disbel = (disbel_a * uncer_b + disbel_b * uncer_a) \
                   / (uncer_a + uncer_b - uncer_a*uncer_b)
            
            uncer =             (uncer_a * uncer_b)           \
                   / (uncer_a + uncer_b - uncer_a*uncer_b)

            if uncer_a != 1 and uncer_b != 1:
                alpha = (alpha_a * uncer_b + alpha_b * uncer_a - (alpha_a + alpha_b) * uncer_a * uncer_b) \
                                     / (uncer_a + uncer_b - 2 * uncer_a * uncer_b) 
            else:
                alpha = (alpha_a + alpha_b) / 2.

        return belif, disbel, uncer, alpha
    
    def evidence(self, answer):
        s = answer
        r = 1-answer
        w = 2

        total = s+r+w

        beliefs    = s / total
        disbeliefs = r / total
        uncertanty = w / total

        return beliefs, disbeliefs, uncertanty, self.alpha
    
    def discount_factor(self, agent_id, evidence):
        belif_a, disbel_a, _, alpha_a = evidence
        
        agent_trust = self.get_trustworthiness(agent_id)

        belif  = belif_a * agent_trust
        disbel = disbel_a * agent_trust
        uncer  = 1 - agent_trust*belif_a - agent_trust*disbel_a
        alpha = alpha_a

        return belif, disbel, uncer, alpha

    def avg_fusion(self, op_a, op_b):
        belif_a, disbel_a, uncer_a, alpha_a = op_a
        belif_b, disbel_b, uncer_b, alpha_b = op_b

        if uncer_a == 0 and uncer_b == 0:
            return self.cumulative_fusion(op_a, op_b)

        belif = (belif_a * uncer_b + belif_b * uncer_a) \
               /          (uncer_a + uncer_b)
        
        disbel = (disbel_a * uncer_b + disbel_b * uncer_a) \
               /            (uncer_a + uncer_b)
        
        uncer = (2 * uncer_a * uncer_b) \
               /  (uncer_a + uncer_b)
        
        alpha = (alpha_a + alpha_b) / 2.

        return belif, disbel, uncer, alpha
        