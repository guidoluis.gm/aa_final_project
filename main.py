from statistics import mode
from random import shuffle
from random import randint
from agent import Agent
import numpy as np


def create_agents(n_agents: int, liar_id) -> list[Agent]:
    # define the number of liars
    agents = [None] * n_agents
    liar_id = randint(0, n_agents)

    for id in range(n_agents):

        if id == liar_id:
            global a
            agent = Agent(id, n_agents, agents, a=a, b=3)
        else:
            agent = Agent(id, n_agents, agents)
        agents[id] = agent
        if id == liar_id:
            agent.liar = True
    
    for agent in agents:
        agent.init_opinion()

    return agents, liar_id

def round_start(agents: list[Agent]) -> None:
    for agent in agents:
        agent.recieved_messages.clear()
        agent.answers.clear()
    shuffle(agents)

def send_questions(agents: list[Agent]) -> None:
    total_questions = 0
    curr_agent = 0
    while total_questions < len(agents):
        total_questions += agents[curr_agent].send_message()
        curr_agent = (curr_agent + 1) % len(agents)

def answer_questions(agents: list[Agent]) -> None:
    for agent in agents:
        agent.answer()
        #print(agent.id)
        #print(agent.answers)


def election(agents: list[Agent]) -> bool:
    votes = [a.vote() for a in agents]
    vote = mode(votes)
    for idx, agent in enumerate(agents):
        if agent.id == vote:
            #print(f'Removing agent {agent.id}')
            agents.pop(idx)
            return agent.id
    return None


def round(agents: list[Agent]) -> bool:

    #print(f'\nRearrange the Agents')
    round_start(agents) # clean the recieved questions

    #print(f'\nSending the questions')
    send_questions(agents) # ask the questions


    # answere the questions
    #print(f'\nAnswering the questions')
    answer_questions(agents)

    #print(f'\nVoting for the liar')


    removed_id = election(agents)
    #print(agents[0].opinions)
    #print(len(agents))

    # returns true if they found the liar
    return removed_id


def main():

    # start the agents
    n_agents = 10

    agents, liar_id = create_agents(n_agents, randint(0, n_agents-1))
    
    for r in range(n_agents-2):
        #print('-'*6 + f' Round: {r} ' + '-' * 6)
        removed_id = round(agents)
        #print()
        if removed_id == liar_id:
            #print(f'found the liar at round: {r}')
            return False


    #print(f'The liar agent, id: {winner.id} won!')
    return True


if __name__ == '__main__':
    global a
    start = 1
    end = 9
    for _a in np.linspace(start, end, 81):
        runs = 10_000
        l_w = 0
        a = _a
        for r in range(runs):
            l_w += main()
        print(f'curve: a: {a}, b: 3')
        print(f'liar wins rate: {l_w/runs}')
        print()